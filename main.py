import csv
import sys
from datetime import date
import matplotlib.pyplot as plt

filename = sys.argv[1]

def value_or_zero(value: str):
    if value == "":
        return 0
    else:
        return float(value.replace(",", "."))

with open(filename) as csv_file:
    csv_reader = csv.reader(csv_file, delimiter=';')
    line_count = 0

    tickets_bought = [0 for _ in range(18)]

    money_tickets_conso = 0

    for i, row in enumerate(csv_reader):
        if i == 0:
            continue
        yea_mon_day = [int(x) for x in row[1].split(' ')[0].split('/')]
        ticket_date = date(year=yea_mon_day[2], month=yea_mon_day[1], day=yea_mon_day[0]) - date(2024, 2, 27)
        tickets_bought[int(ticket_date.days)] += 1

        money_tickets_conso += value_or_zero(row[17]) + value_or_zero(row[19]) + value_or_zero(row[21]) + value_or_zero(row[23])

        line_count += 1

    cumulated_tickets = [tickets_bought[0]]
    for i in range(len(tickets_bought) - 1):
        cumulated_tickets.append(cumulated_tickets[i] + tickets_bought[i+1])

    print(f'Billets achetés : {line_count}.')
    print(f'Total des tickets conso. accumulés : {money_tickets_conso}.')

    plt.plot(list(range(18)), tickets_bought, label="Tickets achetés")
    plt.plot(list(range(18)), cumulated_tickets, label="Tickets cumulés")
    plt.title("Nombre de tickets vendus en fonction du jour")
    plt.xlabel("Jours depuis l'ouverture de la billeterie")
    plt.ylabel("Tickets")
    plt.legend()
    plt.show()